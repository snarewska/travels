package ostra.jazda.travelagency.config;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;


/**
 * Niunka created on 19.05.2020
 */

@Configuration
public class DataSourceConfig {

    @Bean(name = "transactionManager")
    public DataSourceTransactionManager dataSourceTransactionManager(DataSource dataSource){
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean
    public DataSource dataSource(){
        return DataSourceBuilder.create()
                .url("jdbc:mysql://localhost:3306/travelagency?serverTimezone=UTC")
                .username("root")
                .password("niunka")
                .driverClassName("com.mysql.jdbc.Driver")
                .build();

    }

}

