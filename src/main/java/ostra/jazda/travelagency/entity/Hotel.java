package ostra.jazda.travelagency.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * Niunka created on 23.04.2020
 */
@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "hotel")
@NamedQuery(name = "getByName", query = "SELECT h FROM Hotel h WHERE h.name = :name")
public class Hotel {

   @Id
   @GeneratedValue
   private Long idhotel;

   @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
   @JoinColumn(name="idhotel")
   private List<Room> rooms;

   @Column(name = "hotelbeach")
   private boolean hotelBeach;

   @Column(name = "freewifi")
   private boolean freeWifi;
   private int stars;
   private String name;
   private Meals meals;

}


