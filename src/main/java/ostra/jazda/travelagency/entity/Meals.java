package ostra.jazda.travelagency.entity;

public enum Meals {
    ALL_INCLUSIVE,
    BED_AND_BREAKFAST,
    FULL_BOARD,
    SELF_CATERING
}
