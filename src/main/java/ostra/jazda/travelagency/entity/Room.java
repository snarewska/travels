package ostra.jazda.travelagency.entity;

import lombok.*;

import javax.persistence.*;

/**
 * Niunka created on 23.04.2020
 */
@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "room")
@NamedQueries({
        @NamedQuery(name = "getByMaxPeople", query = "SELECT room FROM Room room WHERE room.maxPeople = :maxPeople"),
        @NamedQuery(name = "getByBath", query = "SELECT room FROM Room room WHERE room.bath = :bath"),
        @NamedQuery(name = "getBySeaView", query = "SELECT room FROM Room room WHERE room.seaView = :seaView")
})
public class Room {

    @Id
    @Column(name = "idroom")
    @GeneratedValue
    private Long idroom;

    @Column(name = "maxpeople")
    private int maxPeople;

    @Column(name = "seaview")
    private boolean seaView;
    private boolean bath;
    private boolean balcony;
    private boolean occupied;

    @Column(name = "idhotel")
    private Long idhotel;

    public Room(int maxPeople, boolean seaView, boolean bath, boolean balcony, boolean occupied) {
        this.maxPeople = maxPeople;
        this.seaView = seaView;
        this.bath = bath;
        this.balcony = balcony;
        this.occupied = occupied;
    }
}
