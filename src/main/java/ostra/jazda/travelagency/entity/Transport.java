package ostra.jazda.travelagency.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Niunka created on 23.04.2020
 */

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@NamedQueries({
        @NamedQuery(name = "getByType",
                query = "SELECT t from Transport t where t.type = :type "),
        @NamedQuery(name = "getByFreeLuggage",
                query = "SELECT t from Transport t where t.freeLuggage = :freeLuggage "),
})

public class Transport {

    @Id
    @GeneratedValue
    @Column(name = "idtransport")
    private Long id;
    private String type;
    private Integer duration;
    @Column(name = "freeluggage")
    private boolean freeLuggage;

}
