package ostra.jazda.travelagency.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * Niunka created on 22.04.2020
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "trip")
public class Trip {
   // private Details details;

    @Id
    @GeneratedValue
    @Column(name = "idtrip")
    private Long tripid;

    @OneToOne
    @JoinColumn(name = "idtransport")
    private Transport transport;

    @OneToOne
    @JoinColumn(name = "idhotel")
    private Hotel hotel;

    @OneToOne
    @JoinColumn(name = "idroom")
    private Room room;

    private String country;
    private double price;

    @Column(name = "closebeach")
    private boolean closeBeach;
    private Date arrival;
    private Date departure;
    private int people;




}
