package ostra.jazda.travelagency.hotel;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ostra.jazda.travelagency.entity.Hotel;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * Niunka created on 31.05.2020
 */

@RestController
@RequestMapping("/tagency/hotel")
public class HotelController {

    private final HotelRepo hotelRepo;
    private final HotelService hotelService;

    public HotelController(HotelRepo hotelRepo, HotelService hotelService) {
        this.hotelRepo = hotelRepo;
        this.hotelService = hotelService;
    }

    @RequestMapping(value = "/new", method = GET)
    public void addWithAllExistingRooms(){
        hotelRepo.addWithAllExistingRooms();
    }

    @RequestMapping(value = "/{name}")
    public Hotel getByName(@PathVariable (name = "name") String name){
        return hotelService.getByName(name);
    }

    @RequestMapping(value = "/add", method = POST)
    public void add(){
        hotelRepo.add();
    }
}
