package ostra.jazda.travelagency.hotel;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ostra.jazda.travelagency.entity.Hotel;
import ostra.jazda.travelagency.entity.Room;
import ostra.jazda.travelagency.room.RoomService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Niunka created on 28.05.2020
 */

@Repository
public class HotelRepo {

    @PersistenceContext
    private final EntityManager entityManager;
    private final RoomService roomService;

    public HotelRepo(EntityManager entityManager, RoomService roomService) {
        this.entityManager = entityManager;
        this.roomService = roomService;
    }

    @Transactional
    public void addWithAllExistingRooms(){
        Hotel hotel = new Hotel();
        Query query = entityManager.createQuery("select e from Room e");
        List<Room> rooms = (List<Room>) query.getResultList();
        hotel.setRooms(rooms);
        entityManager.persist(hotel);
    }

    @Transactional
    public void add(){
        Hotel hotelFasanoPuntaDelEste = getHotel("Hotel Fasano Punta del Este", 5, true, true);
        Hotel hotelSunIslandResort = getHotel("Sun Island Resort", 5, true, true);
        Hotel hotelMahiaAndResort = getHotel("Mahia Hotel & Resort", 4, true, false);
       for (Hotel hotel : Arrays.asList(hotelFasanoPuntaDelEste, hotelSunIslandResort, hotelMahiaAndResort)){
           entityManager.persist(hotel);
       }
    }

    private Hotel getHotel(String name, int stars, boolean freeWifi, boolean hotelBeach) {
        Hotel hotel = new Hotel();
        hotel.setName(name);
        hotel.setStars(stars);
        hotel.setFreeWifi(freeWifi);
        hotel.setHotelBeach(hotelBeach);
        List<Room> rooms = createRooms();
        hotel.setRooms(rooms);
        return hotel;
    }

    private List<Room> createRooms() {
        List<Room> rooms = new ArrayList<>();
        rooms.addAll(roomService.createRooms(2, true, true, true, false, 100));
        rooms.addAll(roomService.createRooms(4, false, true, true, false, 50));
        rooms.addAll(roomService.createRooms(1, false, false, false, false, 20));
        rooms.addAll(roomService.createRooms(3, true, true, true, false, 30));
        return rooms;
    }

    @Transactional
    public Hotel getByName(String name) {
        Hotel hotel = (Hotel) entityManager.createNamedQuery("getByName")
                .setParameter("name", name)
                .getSingleResult();
        return hotel;
    }
}

