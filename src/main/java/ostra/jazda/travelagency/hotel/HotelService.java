package ostra.jazda.travelagency.hotel;

import org.springframework.stereotype.Service;
import ostra.jazda.travelagency.entity.Hotel;

/**
 * Niunka created on 07.06.2020
 */

@Service
public class HotelService {

    private final HotelRepo hotelRepo;

    public HotelService(HotelRepo hotelRepo) {
        this.hotelRepo = hotelRepo;
    }

    public Hotel getByName(String name) {
        return hotelRepo.getByName(name);
    }
}
