package ostra.jazda.travelagency.room;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ostra.jazda.travelagency.entity.Room;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * Niunka created on 22.05.2020
 */

@RestController
@RequestMapping("/tagency/room")
public class RoomController {

    private final RoomService roomService;

    @Autowired
    public RoomController(RoomService roomService) {
        this.roomService = roomService;
    }

    @GetMapping
    public List<Room> getRoooms(){
        return roomService.getRooms();
    }

    @RequestMapping(value = "/view", method = GET)
    public List<Room> getRoomsBySeaView(@RequestParam boolean seaView){
        return roomService.getBySeaView(seaView);
    }

    @RequestMapping(value = "/bath", method = GET)
    public List<Room> getRoomsByBath(@RequestParam boolean bath){
        return roomService.getByBath(bath);
    }

    @RequestMapping(value = "/{id}/bath", method = POST)
    public void updateBath(@PathVariable (name = "id") Long id, @RequestParam boolean bath){
        roomService.updateBath(id, bath);
    }

    @RequestMapping(value = "/new", method = POST)
    public void save(@RequestBody Room room){
        roomService.save(room);
    }

    @RequestMapping(value = "/news", method = POST)
    public void saveAll(@RequestBody List<Room> rooms){
        roomService.saveAll(rooms);
    }

    @RequestMapping(value = "/delete/{maxpeople}", method = DELETE)
    public void deleteRoomsByMaxPeople(@PathVariable (name = "maxpeople") int maxPeople){
        roomService.deleteRoomsByMaxPeople(maxPeople);
    }

   /* @RequestMapping(value = "/{seaView}")
    public List<Room> getRoomsBySeaView(@PathVariable (name = "seaView") boolean seaView){
        return roomService.getRoomsBySeaView(seaView);
    }*/
}
