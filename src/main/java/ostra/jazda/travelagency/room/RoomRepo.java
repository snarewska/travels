package ostra.jazda.travelagency.room;

import lombok.extern.java.Log;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ostra.jazda.travelagency.entity.Room;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Niunka created on 22.05.2020
 */
@Log4j2
@Repository
public class RoomRepo  {

    @PersistenceContext
    private final EntityManager entityManager;
    private static final String GET_FREE_ROOMS = "select * FROM room WHERE occupied = false";
    private static final String GET_ROOMS = "select * FROM room";

    public RoomRepo(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Transactional
    public List<Room> getFreeRooms(){
        List<Room> free = (List<Room>) entityManager.createNativeQuery(GET_FREE_ROOMS, Room.class).getResultList();
        return free;
    }

    @Transactional
    public List<Room> getRooms(){
        return (List<Room>) entityManager.createNativeQuery(GET_ROOMS, Room.class).getResultList();

    }

    @Transactional
    public void save(Room room){
        entityManager.persist(room);
    }

    @Transactional
    public List<Room> getByMaxPeople(int maxPeople) {
      return (List<Room>) entityManager.createNamedQuery("getByMaxPeople")
                .setParameter("maxPeople", maxPeople)
                .getResultList();
    }

    @Transactional
    public List<Room> getByBath(boolean bath){
        return (List<Room>) entityManager.createNamedQuery("getByBath")
                .setParameter("bath", bath)
                .getResultList();
    }

    @Transactional
    public Room getById(Long id){
        return entityManager.find(Room.class, id);
    }

    @Transactional
    public List<Room> getBySeaView(boolean seaView){
        return (List<Room>) entityManager.createNamedQuery("getBySeaView")
                .setParameter("seaView", seaView)
                .getResultList();
    }

    @Transactional
    public void deleteAll(List<Room> rooms){
        rooms.forEach(room -> entityManager.remove(room));
    }

    @Transactional
    public void saveAll(List<Room> rooms){
        rooms.stream()
                .peek(room -> log.info("Id Room: " + room.getIdroom() + " saved" ))
                .forEach(room -> entityManager.persist(room));
    }
}
