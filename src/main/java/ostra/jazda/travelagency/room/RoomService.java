package ostra.jazda.travelagency.room;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import ostra.jazda.travelagency.entity.Room;
import ostra.jazda.travelagency.room.exception.NoFreeRoomException;

import java.util.ArrayList;
import java.util.List;

/**
 * Niunka created on 22.05.2020
 */

@Log4j2
@Service
public class RoomService {

    private RoomRepo roomRepo;

    public RoomService(RoomRepo roomRepo) {
        this.roomRepo = roomRepo;
    }

    private void occupy(Room room){
        room.setOccupied(true);
        log.info("Id room: " + room.getIdroom() + " occupied");
        roomRepo.save(room);
    }

    public Room findRoomForTrip(int people) throws NoFreeRoomException {
        List<Room> freeRooms = roomRepo.getFreeRooms();
        Room room = freeRooms.stream()
                .filter(r -> r.getMaxPeople() == people)
                .findFirst()
                .orElseThrow(() -> new NoFreeRoomException("NO FREE ROOM FOR: " + people + " person/people"));
        occupy(room);
        log.info("Room occupied");
        return room;
    }

    public void saveAll(List<Room> rooms){
        roomRepo.saveAll(rooms);
        log.info("Added " + rooms.size() + " room/rooms");
    }

    public void deleteRoomsByMaxPeople(int maxPeople){
        List<Room> rooms = roomRepo.getByMaxPeople(maxPeople);
        roomRepo.deleteAll(rooms);
        log.info("Removed rooms with maxpeople {}", maxPeople);
    }

    public List<Room> createRooms(int maxPeople, boolean seaView, boolean bath, boolean balcony, boolean occupied, int amount){
        List<Room> rooms = new ArrayList<>();
        for (int i = 0; i < amount; i++){
            rooms.add(new Room(maxPeople, seaView, bath, balcony, occupied));
        }
        return rooms;
    }

    public void save(Room room){
        roomRepo.save(room);
        log.info("Added new room for: " + room.getMaxPeople() + " person/people");
    }

    public void updateBath(Long id, boolean bath){
        Room room = roomRepo.getById(id);
        room.setBath(bath);
        roomRepo.save(room);
        log.info("Updated room with id {}", id);
    }

    public List<Room> getByBath(boolean bath){
        return roomRepo.getByBath(bath);
    }

    public List<Room> getBySeaView(boolean seaView){
        return roomRepo.getBySeaView(seaView);
    }

    public List<Room> getRooms(){
        return roomRepo.getRooms();
    }




}
