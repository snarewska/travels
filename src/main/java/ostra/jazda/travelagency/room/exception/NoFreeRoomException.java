package ostra.jazda.travelagency.room.exception;

/**
 * Niunka created on 10.06.2020
 */
public class NoFreeRoomException  extends Exception {
    public NoFreeRoomException(String message) {
        super(message);
    }
}
