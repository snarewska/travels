package ostra.jazda.travelagency.transport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ostra.jazda.travelagency.entity.Transport;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * Niunka created on 21.05.2020
 */

@RestController
@RequestMapping("/tagency/transport")
public class TransportController {

    private final TransportService transportService;

    @Autowired
    public TransportController(TransportService transportService) {
        this.transportService = transportService;
    }

    @GetMapping
    public List<Transport> getTransports(){
        return transportService.getTransports();
    }

    @RequestMapping("/luggage")
    public List<Transport> getTransportsByFreeLuggage(@RequestParam boolean freeLuggage){
        return transportService.getTransportsByFreeLuggage(freeLuggage);
    }

    @RequestMapping(value = "/{type}", method = GET)
    public Transport getTransportsByType(@PathVariable (name = "type") String type){
        return transportService.getTransportsByType(type);
    }

    @RequestMapping(value = "/{type}/luggage", method = POST)
    public void updateFreeLuggage(@PathVariable (name = "type") String type, @RequestParam boolean freeLuggage){
        transportService.updateFreeLuggage(freeLuggage, type);
    }

    @RequestMapping(value = "/new", method = POST)
    public void save(@RequestBody Transport transport){
        transportService.save(transport);
    }

    @RequestMapping(value = "/delete/{type}", method = DELETE)
    public void delete(@PathVariable (name = "type") String type){
        transportService.delete(type);
    }
}
