package ostra.jazda.travelagency.transport;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ostra.jazda.travelagency.entity.Transport;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Niunka created on 21.05.2020
 */

@Repository
public class TransportRepo {

    private final static String GET_TRANSPORTS = "SELECT * from Transport";

    @PersistenceContext
    private final EntityManager entityManager;

    public TransportRepo(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Transactional
    public Transport getByType(String type){
        return (Transport) entityManager.createNamedQuery("getByType")
                .setParameter("type", type)
                .getSingleResult();
    }

    @Transactional
    public List<Transport> getByFreeLuggage(boolean freeLuggage){
        return (List<Transport>) entityManager.createNamedQuery("getByFreeLuggage")
                .setParameter("freeLuggage", freeLuggage)
                .getResultList();
    }

    @Transactional
    public void save(Transport transport){
        entityManager.persist(transport);
    }

    @Transactional
    public void delete(Transport transport){
        entityManager.remove(transport);
    }

    @Transactional
    public List<Transport> getAll(){
        return (List<Transport>) entityManager.createNativeQuery(GET_TRANSPORTS)
                .getResultList();
    }


}
