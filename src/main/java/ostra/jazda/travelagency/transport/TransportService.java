package ostra.jazda.travelagency.transport;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import ostra.jazda.travelagency.entity.Transport;

import java.util.List;

/**
 * Niunka created on 21.05.2020
 */
@Log4j2
@Service
public class TransportService {

    private final TransportRepo transportRepo;

    public TransportService(TransportRepo transportRepo) {
        this.transportRepo = transportRepo;
    }

    public List<Transport> getTransports(){
        return transportRepo.getAll();
    }

    public List<Transport> getTransportsByFreeLuggage(boolean freeLuggage) {
        return transportRepo.getByFreeLuggage(freeLuggage);
    }

    public Transport getTransportsByType(String type){
        return transportRepo.getByType(type);
    }

    public void updateFreeLuggage(boolean freeLuggage, String type){
        Transport transport = transportRepo.getByType(type);
        transport.setFreeLuggage(freeLuggage);
        transportRepo.save(transport);
    }

    public void save(Transport transport){
        transportRepo.save(transport);
        log.info("Add new transport: " + transport.getType());
    }

    public void delete(String type){
        Transport transport = transportRepo.getByType(type);
        transportRepo.delete(transport);
    }
}
