package ostra.jazda.travelagency.trip;

import org.springframework.web.bind.annotation.*;
import ostra.jazda.travelagency.entity.Trip;
import ostra.jazda.travelagency.room.exception.NoFreeRoomException;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Niunka created on 07.06.2020
 */

@RestController
@RequestMapping("tagency/trip")
public class TripController {

    private final TripService tripService;

    public TripController(TripService tripService) {
        this.tripService = tripService;
    }

    @RequestMapping(value = "/new", method = POST)
    public void add(@RequestParam(name = "hotelName") String hotelName,
                    @RequestParam(name = "transportType") String transportType,
                    @RequestParam(name = "country") String country,
                    @RequestParam(name = "price") Double price,
                    @RequestParam(name = "people") int people) throws NoFreeRoomException {
        tripService.createTrip(hotelName, transportType, country, price, people);
    }

    @GetMapping("/trips")
    public List<Trip> getTrips(){
        return tripService.getTrips();
    }

    @RequestMapping(value = "/trips/{country}", method = GET)
    public List<Trip> getTripsByCountry(@PathVariable(name = "country") String country){
        return tripService.getTripsByCountry(country);
    }

    @RequestMapping(value = "/trips/new", method = POST)
    public void add(@RequestBody Trip trip){
        tripService.add(trip);
    }

    @RequestMapping("/trips/delete/{country}")
    public void removeTripsByCountry(@PathVariable(name = "country") String country){
        tripService.removeTripsByCountry(country);
    }

    @RequestMapping(value = "/trips/{country}/{price}", method = POST)
    public void updatePrice(@PathVariable(name = "country") String country, @PathVariable(name = "price") Integer price){
        tripService.updatePrice(country, price);
    }
}
