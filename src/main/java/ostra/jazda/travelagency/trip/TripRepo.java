package ostra.jazda.travelagency.trip;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ostra.jazda.travelagency.entity.Hotel;
import ostra.jazda.travelagency.entity.Transport;
import ostra.jazda.travelagency.entity.Trip;
import ostra.jazda.travelagency.hotel.HotelService;
import ostra.jazda.travelagency.transport.TransportService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import java.util.List;

/**
 * Niunka created on 07.06.2020
 */

@Repository
public class TripRepo {

    private final static String GET_TRIPS = "SELECT * from Trip";

    @PersistenceContext
    private final EntityManager entityManager;

    public TripRepo(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Transactional
    public void save(Trip trip){
        entityManager.persist(trip);
    }

    @Transactional
    public List<Trip> getTrips() {
        return (List<Trip>) entityManager.createNativeQuery(GET_TRIPS, Trip.class).getResultList();
    }

    @Transactional
    public void removeAll(List<Trip> trips){
        trips.forEach(trip -> entityManager.remove(trip));
    }
}

