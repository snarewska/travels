package ostra.jazda.travelagency.trip;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import ostra.jazda.travelagency.entity.Hotel;
import ostra.jazda.travelagency.entity.Room;
import ostra.jazda.travelagency.entity.Transport;
import ostra.jazda.travelagency.entity.Trip;
import ostra.jazda.travelagency.hotel.HotelService;
import ostra.jazda.travelagency.room.RoomRepo;
import ostra.jazda.travelagency.room.RoomService;
import ostra.jazda.travelagency.room.exception.NoFreeRoomException;
import ostra.jazda.travelagency.transport.TransportService;
import ostra.jazda.travelagency.utils.PriceManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Niunka created on 07.06.2020
 */

@Service
@Log4j2
public class TripService {

    private final HotelService hotelService;
    private final TransportService transportService;
    private final TripRepo tripRepo;
    private final RoomService roomService;
    private final PriceManager priceManager;

    public TripService(HotelService hotelService, TransportService transportService, TripRepo tripRepo, RoomService roomService, PriceManager priceManager) {
        this.hotelService = hotelService;
        this.transportService = transportService;
        this.tripRepo = tripRepo;
        this.roomService = roomService;
        this.priceManager = priceManager;
    }

    public void createTrip(String hotelName, String transportType, String country, Double price, int people) throws NoFreeRoomException {
        Hotel hotel = hotelService.getByName(hotelName);

        Transport transport = transportService.getTransportsByType(transportType);
        Room roomForTrip = roomService.findRoomForTrip(people);
        Trip trip = new Trip();
        trip.setHotel(hotel);
        trip.setTransport(transport);
        trip.setCountry(country);
        trip.setPrice(priceManager.calculatePrice(people, price));
        trip.setCloseBeach(true);
        trip.setArrival(new Date());
        trip.setDeparture(new Date());
        trip.setPeople(people);
         trip.setRoom(roomForTrip);
        tripRepo.save(trip);
        log.info("Added trip to: " + country);
        log.info(trip);
    }

    public List<Trip> getTrips(){
        return tripRepo.getTrips();
    }

    public List<Trip> getTripsByCountry(String country) {
        List<Trip> trips = new ArrayList<>();
        for (Trip trip : tripRepo.getTrips()){
            if (trip.getCountry().equalsIgnoreCase(country)){
                trips.add(trip);
            }
        }
        return trips;

        /*    Tutaj to samo streamem
         */
    /*    return travelAgencyRepo.getTrips().stream()
                .filter(trip -> trip.getCountry().equalsIgnoreCase(country))
                .collect(Collectors.toList());*/
    }

    public void removeTripsByCountry(String country){
        // getTrips nadmiarowo, ale bardzo ladnie - to dziala.
        List<Trip> trips = new ArrayList<>();
        for (Trip trip : tripRepo.getTrips()){
            if (trip.getCountry().equalsIgnoreCase(country)){
                trips.add(trip);
            }
        }
        tripRepo.removeAll(trips);
    }

    public void add(Trip trip){
        tripRepo.save(trip);
    }

    public void updatePrice(String country, Integer price){
        List<Trip> tripsByCountry = getTripsByCountry(country);
        for (Trip trip : tripsByCountry){
            trip.setPrice(price);
        }
    }
}
