package ostra.jazda.travelagency.utils;

import org.springframework.stereotype.Component;

/**
 * Niunka created on 14.06.2020
 */
@Component
public class PriceManager {

    public double calculatePrice(int people, double price){
        return people * price;
    }
}
