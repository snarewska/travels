package ostra.jazda;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import ostra.jazda.travelagency.utils.PriceManager;


public class PriceManagerTest {

    @Test
    void calculatePriceTest() {
        int people = 7;
        double price = 200.00;
        double expectedResult = 1400.00;

        PriceManager priceManager = new PriceManager();
        double result = priceManager.calculatePrice(people, price);
        Assert.assertEquals(expectedResult, result, 0.0);
    }
}
